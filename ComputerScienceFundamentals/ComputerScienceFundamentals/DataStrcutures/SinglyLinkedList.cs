using System;
using System.Collections;
namespace DataStructures
{
    public class LinkedListNode<T>
    {
        public T Value{get;set;}
        public LinkedListNode<T> Next{get;set;}
        public LinkedListNode(T value,LinkedListNode<T> next)
        {
            Value= value;
			Next = next;
        }        
    }
	
	public class LinkedList<T> : IEnumerable<T>
	{
        public Node<T> Head{get; private set;}
		
        public Node<T> Tail{get; private set;}
		
        public int Count{get;private set;}
		
        public void AddFront(T value)
        {
            var temp = Head;
			
            Head= new Node(value,temp);
			
            Count++;
			
            if(Count==1)
            {
                Tail = Head;
            }            
            
		}
		
     	public void AddEnd(T value)
     	{
			var node = new LinkedListNode(value,null);
			
        	if(Count==0)
			{
				Head = node;
				
				Head.Next = Tail;
			}   
			else
			{
				Tail.Next = node
			}
			Tail = node;
			
			Count++;
		}
		
		public void Add(T value)
		{
			AddFront(value);
		}
		
		public bool Remove(T value)
		{
			var current = Head;
			
			LinkedListNode<T> previous=null;
			
			while(current != null)
			{
				if(current.Value.equals(value))
				{
					if(previous != null)
					{
						previous.Next = current.Next;
						
						if(current.Next==null)
						{
							Tail = previous;
						}
						
						Count--;
					}
					else
					{
						RemoveFirst();
					}
					return true;
				}
				previous = current;
				current = current.Next;
			}
			
			return false;
		}
        public void RemoveLast()
        {
			if(Count > 0)
			{
				if(Count==1)
				{
					Head = Tail = null;	
				}
				else
				{
					var current = Head;
					while(current.Next!=Tail)
					{
						current=current.Next;
					}
					Tail = current;
					Tail.Next = null;
				}

				Count--;
			}
        }
		
		public void RemoveFirst()
		{
			if(Count > 0 )
			{
				Head = Head.Next;	
				Count--;
				if(Count==0)
				{
					Tail = null;
				}
			}
		}
		
		System.Collections.Generic.IEnumerator<T> System.Collections.Generic.IEnumerable<T>.GetEnumerator()
		{
			var current = Head;
			while(current!=null)
			{
				yeild return current;
				current = current.Next;
			}
		}
		
    }
}