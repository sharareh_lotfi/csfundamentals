using System;
using System.Collections;
using System.Collections.Generic;

namespace Graphs
{
    public class AVLTree<T> : IEnumerable<T>
    where T : IComparable
    {
        internal AVLTreeNode<T> _head;
        private int _count;

        public AVLTreeNode<T> Head
        {
            get
            {
                return _head;
            }
        }
        public void Add(T value)
        {

        }
        public void Remove(T value)
        {

        }
        public bool Contains(T value)
        {
            return false;
        }
        public void Clear()
        {

        }
        public int Count()
        {
            return _count;
        }

        public void PostOrderTraversal(Action<T> action)
        {

        }
        public void PreOrderTraversal(Action<T> action)
        {

        }
        public void InOrderTraversal(Action<T> action)
        {

        }

        public IEnumerator<T> InOrderTraversal()
        {
            throw new NotImplementedException();
        }
        public IEnumerator<T> GetEnumerator()
        {
            return InOrderTraversal();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}