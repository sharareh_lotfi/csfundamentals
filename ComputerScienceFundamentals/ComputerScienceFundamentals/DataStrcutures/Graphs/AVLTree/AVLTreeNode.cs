using System;
using System.Collections;
using System.Collections.Generic;

namespace Graphs
{
    public class AVLTreeNode<TNode> : IComparable<TNode>
    where TNode : IComparable
    {
        private AVLTree<TNode> _tree;
        private AVLTreeNode<TNode> _left;
        public AVLTreeNode<TNode> Left
        {
            get { return _left; }
            internal set
            {
                _left = value;
                if (_left != null)
                {
                    _left.Parent = this;
                }
            }
        }
        private AVLTreeNode<TNode> _right;
        public AVLTreeNode<TNode> Right
        {
            get { return _right; }
            internal set
            {
                _right = value;
                if (_right != null)
                {
                    _right.Parent = this;
                }
            }
        }
        public AVLTreeNode<TNode> Parent { get; internal set; }
        public TNode Value { get; private set; }

        public AVLTreeNode(TNode value, AVLTreeNode<TNode> parent, AVLTree<TNode> tree)
        {
            Parent = parent;
            _tree = tree;
            Value = value;
        }
        public int CompareTo(TNode other)
        {
            return Value.CompareTo(other);
        }

        //Balancing Methods
        internal void Balance()
        {
            if (State == TreeState.RightHeavy)
            {
                if (Right != null && Right.BalanceFactor < 0)
                {
                    LeftRightRotation();
                }
                else
                {
                    LeftRotation();
                }
            }
            else if (State == TreeState.LeftHeavy)
            {
                if (Left != null && Left.BalanceFactor > 0)
                {
                    RightLeftRotation();
                }
                else
                {
                    RightRotation();
                }
            }
        }
        private void LeftRotation()
        {
            var newRoot = this.Right;
            // Replace the Right child of root with root
            ReplaceRoot(newRoot);
            // Left child of the new root is assigned to Right child of the old root
            Right = newRoot.Left;
            // Old root becomes the new root's Left child
            newRoot.Left = this;
        }
        private void RightRotation()
        {
            var newRoot = Left;
            // Replace the Left child of root with root
            ReplaceRoot(newRoot);
            // Right child of the new root is assigned to Left child of the old root
            Left = newRoot.Right;
            // Old root becomes the new root's Right child
            newRoot.Right = this;
        }
        private void LeftRightRotation()
        {
            Right.RightRotation();
            LeftRotation();
        }
        private void RightLeftRotation()
        {
            Left.LeftRotation();
            RightRotation();
        }
        private void ReplaceRoot(AVLTreeNode<TNode> newRoot)
        {
            if (this.Parent != null)
            {
                if (this.Parent.Left == this)
                {
                    this.Parent.Left = newRoot;
                }
                else if (this.Parent.Right == this)
                {
                    this.Parent.Right = newRoot;
                }
            }
            else
            {
                _tree._head = newRoot;
            }
            newRoot.Parent = this.Parent;
            this.Parent = newRoot;
        }
        //Support properties and methods
        private int MaxChildHeight(AVLTreeNode<TNode> node)
        {
            if (node != null)
            {
                return 1 + Math.Max(MaxChildHeight(node.Left), MaxChildHeight(node.Right));
            }
            return 0;
        }
        private int LeftHeight
        {
            get
            {
                return MaxChildHeight(Left);
            }
        }
        private int RightHeight
        {
            get
            {
                return MaxChildHeight(Right);
            }
        }
        private TreeState State
        {
            get
            {
                if (LeftHeight - RightHeight > 1)
                {
                    return TreeState.LeftHeavy;
                }
                if (RightHeight - LeftHeight > 1)
                {
                    return TreeState.RightHeavy;
                }
                return TreeState.Balanced;
            }
        }
        private int BalanceFactor
        {
            get
            {
                return RightHeight - LeftHeight;
            }
        }
    }
    public enum TreeState
    {
        Balanced,
        LeftHeavy,
        RightHeavy,
    }
}